#pragma once

typedef struct {
    uint8_t start, end, _val;
    int8_t value;
    int8_t (*format)(uint8_t);
} s_channel;

s_channel channels[4];


void rcrx_init(void);

// non-blocking, to be called in main loop
void rcrx_main(void);
