/* Stepper motor controller
 *
 *
 * Pin configuration:
 *
 *   328 | Function | Ard. | ext.
 *   ----+----------+------+-----
 *   PB3 | OC2A     | D11  | Power drill PWM output
 *   PD3 | OC2B     | D3   | LEGO motor output
 *
 *   PB0 |          | D8   | stepper nENABLE
 *   PB1 | OC1A     | D9   | stepper STEP
 *   PB2 |          | D10  | stepper DIR
 *   PB4 |          | D12  | stepper nFAULT
 *   PD5 |          | D5   | stepper M0
 *   PD6 |          | D6   | stepper M1
 *   PD7 |          | D7   | stepper M2
 *
 *   PC0 | PCINT8   | A0   | RC CH0 input
 *   PC1 | PCINT9   | A1   | RC CH1 input
 *   PC2 | PCINT10  | A2   | RC CH2 input
 *   PC3 | PCINT11  | A3   | RC CH3 input
 *   PB4 | LED      | D13  | onboard LED (debug)
 *
 *
 */


#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "uart.h"
#include "xitoa.h"

#include "stepper.h"
#include "rcrx-parse.h"

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))
#define limit(a, lower, upper) (min(max(a, lower), upper))


typedef uint8_t bool;

inline void prompt(void) { xputs(PSTR("> ")); }
void set_lego_motor(int8_t val) {
    if (val)
        PORTD |= (1 << PD3);
    else
        PORTD &= ~(1 << PD3);
}
void set_drill_motor(uint8_t val) {
    if (val) {
        OCR2A = 2 * val;
        DDRB |= (1 << PB3);
    } else {
        DDRB &= ~(1 << PB3);
    }
}

int main(void) {
    uart_init(UBRR_VALUE, USE_2X);
    xfunc_out = (void(*)(uint8_t))(uart_tx);

    print("Up and running!");

    // stepper_init();
    rcrx_init();

    // Power drill PWM output
    TCCR2A = (3 << WGM20) | (2 << COM2A0);
    TCCR2B = (2 << CS20);

    // Lego DC brush motor
    DDRD |= (1 << PD3);

    // stepper_set_speed(400);
    prompt();

    // interrupt on UART receive
    UCSR0B |= (1 << RXCIE0);
    sei();

    while (1) {
        rcrx_main();

        // power drill PWM
        set_drill_motor(channels[0].value);

        // LEGO motor
        set_lego_motor(channels[2].value);

        // stepper motor
        stepper_set_speed((int16_t)channels[1].value * 5);
    }
}

typedef struct {
    enum {INVALID, LEGO, DRILL, STEPPER} cmd;
    int arg;
} s_command;
s_command parse_command(char* buffer) {
    s_command command;

    if (buffer[0] > 'Z') buffer[0] -= ('a' - 'A');  // make uppercase

    switch (buffer[0]) {
        case 'L':
            command.cmd = LEGO; break;
        case 'D':
            command.cmd = DRILL; break;
        case 'S':
            command.cmd = STEPPER; break;
        default:
            command.cmd = INVALID;
            return command;
    }

    size_t i = 1;
    while ((buffer[i] < '0' || buffer[i] > '9') && buffer[i] != '-') i++;  // scroll forward

    char* num = (char*)&(buffer[i]);
    // printf("'%s'", num);
    command.arg = atoi(num);

    return command;
}

ISR (USART_RX_vect) {
    static char buffer[16];
    static size_t i;

    char c = UDR0;

    if (c == '\r' || c == '\n') {
        print();  // newline
        if (i > 0) {
            buffer[i++] = 0;
            s_command command = parse_command((char*)buffer);

            switch (command.cmd) {
                case LEGO:
                    set_lego_motor((int8_t)command.arg);
                    printf("Lego motor set to %d", command.arg);
                    break;
                case DRILL:
                    set_drill_motor((uint8_t)command.arg);
                    printf("Drill motor set to %d", command.arg);
                    break;
                case STEPPER:
                    {   // start new scope for `v`
                        int16_t v = stepper_set_speed((int16_t)command.arg);
                        printf("Stepper motor speed set to %d", v);
                    }
                    break;
                case INVALID:
                    printf("Error: invalid command ('%s')", buffer);
            }

            i = 0;
        }
        prompt();
    } else if (c == '?') {
        print("RC channel values:");
        for (int8_t i = 0; i < 4; i++)
            printf("  ch%d: %3d", i, channels[i].value);
        prompt();
    } else if (c >= ' ') {  // this generic rule needs to be last!
        buffer[i++] = c;
        xputc(c);
    }
}
