#include <math.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "rcrx-parse.h"

#define DEBUG


#define RCRX_CH0_MASK   (1 << PC0)
#define RCRX_CH1_MASK   (1 << PC1)
#define RCRX_CH2_MASK   (1 << PC2)
#define RCRX_CH3_MASK   (1 << PC3)
#define PINC_MASK  (RCRX_CH0_MASK | RCRX_CH1_MASK | RCRX_CH2_MASK | RCRX_CH3_MASK)

// TODO: hardware jumper for calibration to EEPROM? per channel?
#define RC_MINVAL       68
#define RC_MAXVAL       120


int8_t map_pm_100(uint8_t val);
int8_t map_0_127(uint8_t val);
int8_t map_switch(uint8_t val);


void rcrx_init(void) {
    // 4ms cycle, 16µs resolution
    TCCR0A = 0;  // no waveform generation, no output compares
    TCCR0B = (4 << CS00);  // start timer with PS256

    // Enable internal pull-up resistors.
    // If we start the system without a receiver connected,
    // the PCINT will never fire, since the inputs aren't floating.
    PORTC |= PINC_MASK;

    // PCINT1 for PC0..3
    PCICR |= (1 << PCIE1);
    PCMSK1 = PINC_MASK;

    channels[0].format = &(map_0_127);
    channels[1].format = &(map_pm_100);
    channels[2].format = &(map_switch);
    channels[3].format = &(map_switch);

#ifdef DEBUG
    DDRB |= (1 << PB5);  // LED
#endif
}

int8_t map_pm_100(uint8_t val) {
    const uint8_t valmin = RC_MINVAL, valmax = RC_MAXVAL;
    const int8_t mapmin = -100, mapmax = 100, dz = 5;
    const int8_t offset = (valmin + valmax) / 2;
    const float k = (float)((uint8_t)mapmax - mapmin) /
                           ((uint8_t)valmax - valmin - 2);

    if (val <= (valmin + 1)) return mapmin;
    if (val >= (valmax - 1)) return mapmax;

    int8_t value = round(k * (val - offset));
    if ((value >= 0 ? value : -value) < dz) return 0;
    return value;
}

int8_t map_0_127(uint8_t val) {
    const uint8_t valmin = RC_MINVAL, valmax = RC_MAXVAL, mapmax = 127, dz = 10;
    const uint8_t offset = valmin + 1;
    const float k = (float)(mapmax) / (valmax - valmin - 2);

    if (val <= (valmin + 1)) return 0;
    if (val >= (valmax - 1)) return mapmax;

    int8_t value = round(k * (val - offset));
    if (value < dz) return 0;
    return value;
}

int8_t map_switch(uint8_t val) {
    const uint8_t valmin = RC_MINVAL, valmax = RC_MAXVAL;
    const uint8_t step = (valmax - valmin + 1) / 3;

    if (val < (valmin + step)) return -1;
    if (val > (valmax - step)) return  1;
    return 0;
}

void update_channel(s_channel* ch) {
    if (ch->_val != 0)
        ch->value = (ch->format ? ch->format(ch->_val) : ch->_val);
}

void rcrx_main(void) {
    for (int8_t i = 0; i < 4; i++)
        update_channel(&(channels[i]));
}


int8_t active_channel(char pin) {
    if (pin & RCRX_CH0_MASK) return 0;
    if (pin & RCRX_CH1_MASK) return 1;
    if (pin & RCRX_CH2_MASK) return 2;
    if (pin & RCRX_CH3_MASK) return 3;
    return -1;
}

s_channel* ch;
ISR(PCINT1_vect) {
    uint8_t t = TCNT0;  // snapshot
    char pind = PINC & PINC_MASK;

    if (pind) {
        // rising edge, given that only one is high at any time
        int8_t ix = active_channel(pind);
        if (ix >= 0) {
            ch = &(channels[ix]);
            ch->start = t;
#ifdef DEBUG
            PORTB |= (1 << PB5);  // LED
#endif
        } else {
            ch = 0;
        }
    } else {
        // falling edge, given that only one is high at any time
        if (ch) {
            ch->end = t;
            ch->_val = ch->end - ch->start;
        }
#ifdef DEBUG
        PORTB &= ~(1 << PB5);  // LED
#endif
    }
}
