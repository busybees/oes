#pragma once


typedef uint8_t bool;

void stepper_init(void);

// TODO: what is speed?
//       currently: full step frequency
//       later maybe mm/s
int16_t  stepper_set_speed(int16_t speed);

bool stepper_status_OK(void);
