#include <stdlib.h>
#include <avr/io.h>

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

#include "stepper.h"
#include "xitoa.h"

/*
 *   PB0 |          | D8   | stepper nENABLE
 *   PB1 | OC1A     | D9   | stepper STEP
 *   PB2 |          | D10  | stepper DIR
 *   PB4 |          | D12  | stepper nFAULT
 *   PD5 |          | D5   | stepper M0
 *   PD6 |          | D6   | stepper M1
 *   PD7 |          | D7   | stepper M2
 * */

#define STEPPER_DISABLE PB0
#define STEPPER_STP     PB1
#define STEPPER_DIR     PB2
#define STEPPER_OK      PB4

#define STEPPER_M0      PD5
#define STEPPER_M1      PD6
#define STEPPER_M2      PD7

#define STEPPER_MODE_MASK (0b111 << STEPPER_M0)


// motor-specific values
#define STEPS_PER_REVOLUTION 200
#define SPEED_MAX            (5 * 100)
#define F_S_MAX              2000
#define F_S_MIN              10


typedef enum {OFF = 0, PS1, PS8, PS64, PS256, PS1024} e_prescaler;

inline bool stepper_status_OK(void) { return PINB & (1 << STEPPER_OK); }
inline void clear_prescaler(void) {
    TCCR1B &= ~(0b111 << CS10);
}
inline void set_prescaler(e_prescaler ps) {
    clear_prescaler();
    TCCR1B |= (ps << CS10);
}
inline void clear_mode(void) {
    PORTD &= ~STEPPER_MODE_MASK;
}
inline void set_mode(uint8_t m) {
    clear_mode();
    PORTD |= m << STEPPER_M0;
}
inline void set_direction(int8_t dir) {
    if (dir < 0) PORTB |=  (1 << STEPPER_DIR);
    else         PORTB &= ~(1 << STEPPER_DIR);
}
inline void enable(void) {
    PORTB &= ~(1 << STEPPER_DISABLE);
}
inline void disable(void) {
    PORTB |=  (1 << STEPPER_DISABLE);
    clear_prescaler();
    TCNT1 = 0;
}
int8_t fastlog2(uint32_t);
void set_timer(uint16_t ocr, int8_t dir, e_prescaler ps, uint8_t mode);



int16_t set_frequency(int16_t fs);



void stepper_init(void) {
    // toggle OC1A on compare
    TCCR1A |= 1 << COM1A0;
    // CTC on OCR1A
    TCCR1B |= 1 << WGM12;

    disable();

    DDRD |= STEPPER_MODE_MASK;
    DDRB |= (1 << STEPPER_STP) | (1 << STEPPER_DIR) | (1 << STEPPER_DISABLE);
}


/* Set Motor speed.
 *
 * Currently "speed" is `RPM * 100`
 * TODO: speed should be mm/s
 * */
int16_t stepper_set_speed(int16_t rpm) {
    if (rpm >  SPEED_MAX) rpm =  SPEED_MAX;
    if (rpm < -SPEED_MAX) rpm = -SPEED_MAX;
    printf("rps = %d", rpm);

    // f = rpm / 100 * K
    int16_t f = set_frequency((int16_t)rpm * STEPS_PER_REVOLUTION / 100);
    // rpm = f / K * 100
    return f * 100 / STEPS_PER_REVOLUTION;
}

/* Set Stepper Motor full-step frequency.
 * */
int16_t set_frequency(int16_t fs) {
    int8_t direction;
    if (fs < 0) {
        direction = -1;
        fs = -fs;
    } else {
        direction = 1;
    }
    printf("dir = %d", direction);

    if (fs > F_S_MAX) fs = F_S_MAX;
    printf("fs = %d", fs);

    if (fs < F_S_MIN) {
        disable();
        print("fs < F_S_MAX, disabling.");
        return 0;
    } else {
        e_prescaler ps;
        uint8_t m = 5;
        uint32_t pre_ocr = F_CPU / (2 * fs);
        uint16_t K;
        // TODO: documentation!
        printf("pre_ocr = %lu", pre_ocr);

        int8_t pm = fastlog2(pre_ocr) - 10;
        if (pm < 0) pm = 0;
        printf("p + m = %d", pm);

        if (pm > 11) {
            ps = PS256;      // p = 8
            K = 1 << 13;     // 13 = 8 + 5
        } else if (pm > 8) {
            ps = PS64;       // p = 6
            K = 1 << 11;     // 11 = 6 + 5
        } else if (pm > 5) {
            ps = PS8;        // p = 3
            K = 1 << 8;      // 8 = 3 + 5
        } else {
            ps = PS1;        // p = 0
            m = max(pm, 0);  // `-> pm = m
            K = 1 << m;
        }
        printf("f_us = %d", fs << m);

        uint16_t ocr = pre_ocr / K - 1;

        set_timer(ocr, direction, ps, m);
        return F_CPU / (pre_ocr * 2) * direction;
    }
}


void set_timer(uint16_t ocr, int8_t dir, e_prescaler ps, uint8_t mode) {
    printf("ocr = %u", ocr);
    printf("ps = %u", ps);
    printf("mode = %u", mode);

    if (TCCR1B & (0b111 << CS10)) {  // if timer is running
        TIFR1 |= (1 << OCF1A);  // clear interrupt flag
        loop_until_bit_is_set(TIFR1, OCF1A);
    }

    set_prescaler(ps);
    OCR1A = ocr;
    set_mode(mode);
    set_direction(dir);
}

int8_t fastlog2(uint32_t x) {
    int8_t i = -1;
    while (x) {
        i++; x >>= 1;
    }
    return i;
}
